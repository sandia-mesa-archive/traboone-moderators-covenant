# Traboone Social Moderators' Covenant

We, the adminstrators and moderators, of Traboone Social will commit to the following:

1. **Administration and moderation will be handled as neutrally as possible.** - All reports and widespread information on specific instances and users (often known as bad actors) will be verified and actions will be taken as necessary.
2. **Moderating our own users will be our top priority.** - Our greatest priority will be in moderating our own users before the users of other instances.
3. **Actions taken against other instances and their users will respond to the bare minimum to keep federation going and not influence the ability of Traboone Social's users to connect to other people.** - This means that when it comes to handling other instances and their users, we will take the following approach:
    - If another instance's user spreads content of questionable legality in the United States, we may force the specific posts in question or the user to be unlisted.
    - If another instance's user is posting content illegal in the United States, spamming, or doxing, we will deactivate them and notify the administrators/moderators of the other instance about them.
    - If another instance does not take action against or fails to demonstrate competence in handling users posting illegal content, spamming, or doxing, a full instance block might be the last resort.
4. **All actions taken by administrators and moderators will be communicated to the public.** - The adminstrators and moderators of Traboone Social will be transparent with the public about any actions taken against users and other instances. We will disclose any instance blocks, along with reasoning, in a readable table on the About page.
5. **Administrators and moderators will not use blocklists or recommend blocks.** - Blocklists and block recommendations lack accurate verification but can be taken for granted. We will not use other external blocklists from elsewhere to handle blocks.
6. **No blocking of other instances on the basis of guilt-by-association/federation.** - If administrators and moderators have applied any action against Instance X, an Instance Y that federates with them will not get the same judgement applied to them. A natural connection, as by the fediverse network's design, does not imply shared interests and thus doesn't warrant the same moderation methods.

## Enforcement

With the Traboone Social administrators and moderators' power comes great responsibility. When enforcing the Moderators' Covenant, we will take the following approach:
- If an administrator or moderator has been caught blocking or deactivating a user without reasoning, we may give them the benefit of the doubt, but we will kindly remind them to disclose their reasoning to the public.
- If an adminstrator or moderator has been caught intentionally hiding the blocking of an instance, the banning or deactivation of a certain user, and/or the reasoning behind such decisions, we will temporarily suspend them from administering/moderating Traboone Social until they disclose to the public what they did and the reasoning.
- If an administrator or moderator has been caught intentionally banning or deactivating a user or instance knowing they had no reason to, we will reverse the action in question and may suspend the administrator or moderator from administering/moderating Traboone Social for up to a week.
- If an administrator or moderator has been caught intentionally doing any of the activity as an adminstrator or moderator repeatedly, we will permanently suspend them from administering/moderating Traboone Social.

If you feel that a decision made by a Traboone Social administrator or moderator is unfair or have reason to believe that you were quietly banned or deactivated or had your instance blocked, please email [info@sandiamesa.com](mailto:info@sandiamesa.com).

*This agreement for the administrators and moderators of Traboone Social is based on the [Traboone Social Moderators' Covenant](https://code.sandiamesa.com/traboone/moderators-covenant), which is slightly adapted from [The Fediverse-Friendly Moderation Covenant](https://github.com/pixeldesu/fediverse-friendly-moderation-covenant) and licensed under CC0.*
