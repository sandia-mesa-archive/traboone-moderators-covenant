# Traboone Social Moderators' Covenant

As Traboone Social grows and the number of moderators needed to keep up grows, we want to prevent admin/moderation abuse and keep federation with other instances (the greatest advantage we have over the likes of Facebook, Twitter, and Gab) intact. Slightly adapted from [The Fediverse-Friendly Moderation Covenant](https://github.com/pixeldesu/fediverse-friendly-moderation-covenant), we have written this covenant which all Traboone Social adminstrators and moderators must agree to.

For those looking to adapt our covenant, this should not be seen as a replacement to an instances' set of rules, but rather an extension that checks and balances the power of the administrators and moderators of an instance. The covenant is found in [covenant.md](covenant.md). Simply take the raw markdown or text format (you can find the HTML formatted version the ``other-formats`` directory), adjust it for your needs, and copy it in your instance's rules/terms.

## Contributing

Do you have any suggestions for the covenant? Is there any wrong wording or messed up grammar? Feel free to open an issue or make a pull request improving the covenant!

**Note:** Do not edit the files in the ``other-formats`` folder directly. Instead, use the following method to update the other formats (requires [Node.js](https://nodejs.org/en/download/) to be installed):

```
# Install dependencies
npm install -g yarn
yarn

# Build/update the other formats
yarn build
```

## Who's using the Moderators' Covenant?

Currently, [Traboone Social](https://traboone.com) is the only one using this covenant. However, if you're also using our covenant and want to be shown in this section, feel free to open a pull request to add your instance here or contact [seanking@traboone.com](https://traboone.com/@seanking).

## License

The Traboone Social Moderators' Covenant is licensed under [CC0](LICENSE).
